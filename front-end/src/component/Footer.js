import React from "react";

import "./Footer.css";
// import MediaQuery from "react-responsive";
// import NavLink from "./NavLink";

const Footer = props => (
  <div className="w-100 sticky-bottom footer  pl-3">
    &copy; 2019 Firman Sandy, Naofal Leo Agusta, Cahyadi Hartanto
  </div>
);

export default Footer;
