import React from "react";

import "./Footer.css";
// import MediaQuery from "react-responsive";
// import NavLink from "./NavLink";
import { Row, Col, OverlayTrigger } from "react-bootstrap";
import Popover from "react-bootstrap/Popover";
import Truncate from "react-truncate";
import "./ResultItem.css";
import Swal from "sweetalert2";

class ResultItem extends React.Component {
  handleClick = e => {
    e.stopPropagation();
  };

  handleRelevant = param => {
    Swal.fire({
      icon: "success",
      title: "Thank You For The Positive Feedback!",
      showConfirmButton: false,
      timer: 1500
    });
  };

  handleNotRelevant = param => {
    Swal.fire({
      icon: "error",
      title: "We're So Sorry To Hear That..",
      text: "thank you for the feedback, we'll try our best!",
      showConfirmButton: false,
      timer: 1500
    });
  };

  render() {
    return (
      <Row className="result-item-row">
        <Col xs={11} className="result-item-title" onClick={this.props.method}>
          {this.props.title}
        </Col>
        <OverlayTrigger
          rootClose
          trigger="click"
          placement="right"
          overlay={
            <Popover className="pop-over-wrapping text-center">
              <div>Is This Answer Relevant?</div>
              <Row className="text-center mt-3">
                <Col xs={6}>
                  <i
                    className="fa fa-check check"
                    onClick={this.handleRelevant}
                  ></i>
                </Col>
                <Col xs={6}>
                  <i
                    className="fa fa-times times"
                    onClick={this.handleNotRelevant}
                  ></i>
                </Col>
              </Row>
            </Popover>
          }
        >
          <Col xs={1} className="survey">
            <i className="fa fa-question-circle float-right"></i>
          </Col>
        </OverlayTrigger>
        <Col xs={12} className="result-content" onClick={this.props.method}>
          <Truncate lines={2}>{this.props.content}</Truncate>
        </Col>
        {this.props.score && (
          <Col xs={12} className="result-content" onClick={this.props.method}>
            Score : {this.props.score}
          </Col>
        )}
      </Row>
    );
  }
}

export default ResultItem;
