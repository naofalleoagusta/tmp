import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./Home.css";
import {
  Row,
  Col,
  Form,
  InputGroup,
  Button,
  OverlayTrigger
} from "react-bootstrap";
import Popover from "react-bootstrap/Popover";
import "./SearchResult.css";
import ResultItem from "../component/ResultItem";
//import docStore from "../store/DocStores";
import axios from "axios";
class SearchResult extends Component {
  state = {
    value: "",
    suggestions: [],
    filter: 0,
    title: "",
    content: "",
    dataset: "",
    match: "",
    and: false,
    method: ""
  };
  componentDidMount = () => {
    window.scrollTo(0, 0);
    console.log(this.props.location.state);
    axios
      .get(`http://127.0.0.1:5000/documents`)
      .then(res => {
        this.setState(
          {
            value: this.props.location.state.value,
            match: this.props.location.state.result,
            dataset: res.data,
            and: this.props.location.state.and,
            method: this.props.location.state.method
          },
          () => {
            console.log(this.state);
          }
        );
        // console.log(dataset);
        // dataset = json;
        // console.log(dataset);
        // dataset = JSON.parse(json);
        // console.log(dataset);
      })
      .catch(error => new Error(error.message || error));

    console.log(this.state);
  };
  showFile = (title, content) => {
    this.setState(
      {
        title: title,
        content: content
      },
      () => {
        document.getElementById("content-show").style.display = "block";
        var str = document.getElementById("right-content").innerHTML;
        var search = this.state.value; // for finding inputs value use ".value"
        var twoWord = this.state.value.split(" ");
        // if (twoWord.length <= 1) {
        //   console.log(twoWord);
        //   var regex = new RegExp(search, "g");
        //   var res = str.replace(regex, "<b>" + search + "</b>");
        //   search = search.toLowerCase();
        //   regex = new RegExp(search, "i");
        //   var result = res.replace(regex, "<b>" + search + "</b>");
        //   document.getElementById("right-content").innerHTML = result;
        // } else {
        //   var regex = new RegExp(twoWord[0], "g");
        //   var res = str.replace(regex, "<b>" + search + "</b>");
        //   search = search.toLowerCase();
        //   regex = new RegExp(twoWord[0].toLowerCase(), "i");
        //   var result = res.replace(regex, "<b>" + search + "</b>");
        //   regex = new RegExp(twoWord[1], "g");
        //   result = res.replace(regex, "<b>" + search + "</b>");
        //   regex = new RegExp(twoWord[1].toLowerCase(), "i");
        //   result = res.replace(regex, "<b>" + search + "</b>");
        //   document.getElementById("right-content").innerHTML = result;
        // }
      }
    );
  };

  handleOnClick = () => {
    console.log("masuk");
    window.scrollTo(0, 0);

    if (!this.state.and) {
      if (this.state.method === "db-scan") {
        axios
          .get(`http://127.0.0.1:5000/dbscan`, {
            params: {
              s: this.state.value,
              type: "or"
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: this.state.method,
                  and: false
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      } else {
        axios
          .get(`http://127.0.0.1:5000/k-means`, {
            params: {
              s: this.state.value,
              type: "or"
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: this.state.method,
                  and: false
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      }
    } else {
      if (this.state.method === "db-scan") {
        axios
          .get(`http://127.0.0.1:5000/dbscan`, {
            params: {
              s: this.state.value,
              type: "and"
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: this.state.method,
                  and: true
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      } else {
        axios
          .get(`http://127.0.0.1:5000/k-means`, {
            params: {
              s: this.state.value,
              type: "and"
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: this.state.method,
                  and: true
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      }
    }
  };

  onKeyPress = e => {
    if (this.state.value !== "") {
      if (e.which === 13) {
        this.handleOnClick();
      }
    }
  };

  SearchBar = () => {
    return (
      <InputGroup className="">
        <Form.Control
          className="search-bar m-0"
          placeholder="Type something!"
          onChange={e => {
            this.setState({
              value: e.target.value
            });
          }}
          onKeyDown={this.onKeyPress}
          required
          value={this.state.value}
          type="text"
        />
        <InputGroup.Append>
          <Button variant="outline-secondary" onClick={this.handleOnClick}>
            <i className="fa fa-search"></i>
          </Button>
        </InputGroup.Append>
      </InputGroup>
    );
  };

  RenderAllText = props => {
    // console.log(props.dataset);
    console.log(this.props.location.state);
    console.log(props.match);
    let { method } = this.props.location.state;
    let prop = [];
    console.log(props.match);
    let obj = props.match.data;
    var size = 0,
      key;
    let docs = 0;
    for (key in obj) {
      if (obj.hasOwnProperty(key)) {
        size++;
        docs += obj[key].length;
      }
    }
    if (this.state.method === "k-means") {
      if (size > 0) {
        prop.push(
          <p>
            About {size} cluster(s) and {docs} document(s) found in{" "}
            {props.match.time} milliseconds
          </p>
        );
        for (let i = 1; i <= size; i++) {
          // for(let k=1;k<=props.match.length;k++){

          prop.push(<h2>Cluster {i}</h2>);
          let test = obj[i];
          for (let k = 0; k < test.length; k++) {
            let score = test[k].score;
            let index = test[k].index;
            console.log(score);
            console.log(index);
            prop.push(
              <ResultItem
                key={i}
                match
                title={props.dataset[index].title}
                content={props.dataset[index].text}
                method={() => {
                  this.showFile(
                    props.dataset[index].title,
                    props.dataset[index].text
                  );
                }}
                score={score}
              />
            );
          }

          prop.push(<hr />);
        }
      } else {
        prop.push(<p>No document(s) found!.</p>);
      }
    } else {
      if (size > 0) {
        prop.push(
          <p>
            About {size} cluster(s) and {docs} document(s) found in{" "}
            {props.match.time} milliseconds
          </p>
        );
        for (let i = 0; i < size; i++) {
          // for(let k=1;k<=props.match.length;k++){

          prop.push(<h2>Cluster {i + 1}</h2>);
          let test = obj[i];
          console.log(test);
          for (let k = 0; k < test.length; k++) {
            let score = test[k].score;
            let index = test[k].index;
            console.log(score);
            console.log(index);
            prop.push(
              <ResultItem
                key={i}
                match
                title={props.dataset[index].title}
                content={props.dataset[index].text}
                method={() => {
                  this.showFile(
                    props.dataset[index].title,
                    props.dataset[index].text
                  );
                }}
                score={score}
              />
            );
          }

          prop.push(<hr />);
        }
      } else {
        prop.push(<p>No document(s) found!.</p>);
      }
    }

    return prop;
  };
  render() {
    return (
      <div>
        <Row className="py-2 w-100 sticky-top mx-0 header-result px-5 d-flex align-items-center">
          <Col
            xs={3}
            md={2}
            className="p-0 result-title"
            onClick={() => {
              this.props.history.push("/");
            }}
          >
            <div className="">
              AlticaSearch.
              <small>A PTKI Project.</small>
            </div>
          </Col>
          <Col xs={9} md={6} className="p-0 d-flex align-items-center">
            <this.SearchBar />
          </Col>
        </Row>
        <div className="search-result ">
          <Row className="w-100">
            {this.state.method === "k-means" && (
              <Col xs={6}>
                {/* {this.state.match.lengthTF > 0 ? (
                  <> */}
                {/* <p className="mt-4">
                      About {this.state.match.tfidf.length} result(s) in{" "}
                      {this.state.match.tfidf.time} milliseconds
                    </p> */}
                <this.RenderAllText
                  match={this.state.match}
                  dataset={this.state.dataset}
                />
                {/* </>
                ) : (
                  <h2>No doc(s) match!</h2>
                )} */}
              </Col>
            )}
            {this.state.method === "db-scan" && (
              <Col xs={6}>
                {/* {this.state.match.lengthTF > 0 ? (
                  <> */}
                {/* <p className="mt-4">
                      About {this.state.match.tfidf.length} result(s) in{" "}
                      {this.state.match.tfidf.time} milliseconds
                    </p> */}
                <this.RenderAllText
                  match={this.state.match}
                  dataset={this.state.dataset}
                />
                {/* </>
                ) : (
                  <h2>No doc(s) match!</h2>
                )} */}
              </Col>
            )}
            <Col xs={5}>
              <div id="content-show" className="content">
                <div xs={12} className="title-content">
                  {this.state.title}
                </div>
                <div id="right-content" className="right-content">
                  {this.state.content}
                </div>
              </div>
            </Col>
          </Row>
          {/* <h1>TEST</h1>
          asdasndhaldnkasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa */}
        </div>
      </div>
    );
  }
}
export default withRouter(SearchResult);
